/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, View, SafeAreaView} from 'react-native';
import API from './app/api/wooCommerce';
import { Provider } from "react-redux";
import RootStack from "./app/navigation/navigation";
import store from "./app/redux/configureStore";
import './app/I18n/I18n' // keep before RootContainer

const styles = StyleSheet.create({
  full: {
    position: "absolute", top: 0, bottom: 0, left: 0, right: 0
  }

});

export default class App extends Component<Props> {

  componentWillMount (): void {
    API.login("test8@gmail.com", "qweqwe");
  }

  render() {
    return (
        <View style={styles.full}>
          <SafeAreaView />
          <Provider store={store}>
            <RootStack/>
          </Provider>
        </View>
    );
  }
}