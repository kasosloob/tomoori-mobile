const COLORS = {
  grapePurple: "rgb(86, 26, 40)",
  purpleyGrey: "#949793",
  grapePurpleOpaque: "rgba(86, 26, 30, 1.0)",
  textGray: "rgba(101, 109, 121, 1.0)",
  textDarkGrey: "#424242",
  grayBackgroundColor: "#ececec", 
  textBackgroundColor: "rgba(73, 81, 38, 1.0)",
  lineColor : "rgba(0,0,0,0.2)",
  textColor : "rgb(52, 53, 56)",
  tab1_buttonColor : "rgb(255, 41, 70)",
  tab2_buttonColor : "rgb(73, 81, 38)",
  tab3_buttonColor : "rgb(217, 239, 130)",
  tab4_buttonColor : "rgb(255, 222, 59)",
  notiCellBackgroundColor: "rgba(86, 26, 40, 0.07)",
  whosusTopBackgroundColor: "rgba(86, 26, 40, 0.78)",
};

export default COLORS;
