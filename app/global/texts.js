const TEXTS = {
  welcome: 'مرحبا بك', //welcome
  please_sign_in: 'يرجى تسجيل الدخول للمتابعة', //Please sign in to continue
  login: 'تسجيل الدخول', //log in
  forgot_password:'هل نسيت كلمة المرور؟', //did you forget your password?
  email:'البريد الإلكتروني', //E-mail
  password:'كلمة المرور', //password
  confirm_pwd:'أعد كتابة كلمة المرور', //Rewrite your password
  phonenumber:'رقم الهاتف',
  sex:'الجنس',
  start_now:'ابدأ الآن', //start now
  register_account: '<    تسجيل حساب' //Register an account >
};

export default TEXTS;
