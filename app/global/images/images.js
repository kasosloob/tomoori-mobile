import LOGO from "../../../assets/images/logo.imageset/Logo.png"

import ONBOARD1 from "../../../assets/icons/onboard1.png"
import ONBOARD2 from "../../../assets/icons/onboard2.png"
import ONBOARD3 from "../../../assets/icons/onboard3.png"

//SideMenu
import IC_NOtIFY from "../../../assets/icons/ic_notification.png"
import IC_GETKNOW from "../../../assets/icons/ic_getknow.png"
import IC_EXPLORER from "../../../assets/icons/ic_explorer.png"
import IC_CALL from "../../../assets/icons/ic_call.png"
import IC_BRANCH from "../../../assets/icons/ic_branch.png"
import IC_SETTING from "../../../assets/icons/ic_setting.png"
import IC_CONTACT from "../../../assets/icons/ic_contact.png"
import IC_SECURITY from "../../../assets/icons/ic_security.png"
import TEMP_AVATAR from "../../../assets/icons/ic_temp_avatar.png"
import IC_PLACE from "../../../assets/icons/ic_place.png"
import IC_LOGOUT from "../../../assets/icons/ic_logout.png"
import IC_HOME from "../../../assets/icons/ic_home.png"

import ADS1 from "../../../assets/images/home/sweet_dates_3CH7J.png"
import khudry from "../../../assets/images/home/khudry.png"
import majdoul from "../../../assets/images/home/majdoul.png"
import ajwah from "../../../assets/images/home/ajwah.png"
import safawy from "../../../assets/images/home/safawy.png"
import DSC from "../../../assets/images/home/DSC.png"
import DSC_1079 from "../../../assets/images/home/DSC_1079.png"
import DSC_1088 from "../../../assets/images/home/DSC_1088.png"
import DSC_1092 from "../../../assets/images/home/DSC_1092.png"
import DSC_1096 from "../../../assets/images/home/DSC_1096.png"
import DSC_1102 from "../../../assets/images/home/DSC_1102.png"
import cart from "../../../assets/images/home/cart.png"
import heart from "../../../assets/images/home/heart.png"
import IC_MENU from "../../../assets/icons/ic_menu.png"
import IC_BOOK from "../../../assets/icons/ic_bookmark.png"

import IC_FACEBOOK from "../../../assets/icons/ic_fb_blue.png"
import IC_GOOGLE from "../../../assets/icons/ic_google_red.png"
import IC_ALLOW_RIGHT from "../../../assets/icons/ic_nav_next.png"

import TAB_HOME from "../../../assets/icons/tab_home.png"
import TAB_SEARCH from "../../../assets/icons/tab_search.png"
import TAB_PROFILE from "../../../assets/icons/tab_profile.png"
import TAB_ORDER from "../../../assets/icons/tab_order.png"

import IC_HEART from "../../../assets/icons/ic_heart.png"
import IC_Niti_RED from "../../../assets/icons/ic_noti_red.png"

import IMG_WHOUS1 from "../../../assets/icons/img_whos.png"
import IMG_WHOUS2 from "../../../assets/icons/img_video.png"

import FLAG_AR from "../../../assets/icons/ar.png"
import FLAG_EN from "../../../assets/icons/en.png"


const IMAGES = {
  LOGO,
  ONBOARD1,
  ONBOARD2,
  ONBOARD3,
  IC_NOtIFY,
  IC_GETKNOW,
  IC_EXPLORER,
  IC_CALL,
  IC_BRANCH,
  IC_SETTING,
  IC_CONTACT,
  IC_SECURITY,
  TEMP_AVATAR,
  IC_PLACE,
  IC_LOGOUT,
  IC_HOME,
  ADS1,
  khudry,
  majdoul,
  ajwah,
  safawy,
  DSC,
  DSC_1079,
  DSC_1088,
  DSC_1092,
  DSC_1096,
  DSC_1102,
  cart,
  heart,
  IC_MENU,
  IC_BOOK,
  IC_FACEBOOK,
  IC_GOOGLE,
  IC_ALLOW_RIGHT,
  TAB_HOME,
  TAB_PROFILE,
  TAB_SEARCH,
  TAB_ORDER,
  IC_HEART,
  IC_Niti_RED,
  IMG_WHOUS1,
  IMG_WHOUS2,
  FLAG_EN,
  FLAG_AR
}

export default IMAGES;
