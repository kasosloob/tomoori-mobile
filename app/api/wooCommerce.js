import * as axios from 'axios';

class API {

  baseURL: string;

  expTime: Date;

  constructor() {
    this.baseURL = 'https://tomoori.sa/dev/wp-json/tomapi/v1';
  }

  makeRequest = async ({
                         url,
                         postData,
                       }) => {
    try {
      const response = await axios.post({
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        data: postData || null,
        method: post,
      });
      return response;
    } catch (err) {
      return err;
    }
  };

  login = async (email, password) => {
    const response = await axios.post(`${this.baseURL}/login`, {
      email: email, password:  password})
    console.log('---------- login -------------');
    console.log('response:', response.data);
    return response.data;
  }

  ////
  whoami = async () => {
    const response = await axios.post(`${this.baseURL}/whoami`, {
      refreshtoken: 'qwerty'})
    console.log('whoami', response);
    return response.data;
  }

  ////
  logout = async () => {
    const response = await axios.post(`${this.baseURL}/whoami`, {
      refreshtoken: 'qwerty'})
      //TODO maybe change it to token?
    console.log('logout', response);
    return response.data;
  }

  registeruser = async (username, email, password) => {
    const response = await axios.post(`${this.baseURL}/registeruser`, {
      username: username,
      email: email,
      password: password
    })
    console.log('---------- registeruser -------------');
    console.log('registeruser', response.data);
    return response.data;
  }

  resetpassword = async () => {
    const response = await axios.post(`${this.baseURL}/resetpassword`, {
      email: "useremail@domain.com"
    })
    console.log('resetpassword', response);
    return response.data;
  }

  getProducts = async () => {
    const response = await axios.get(`${this.baseURL}/listproducts`, {headers: {
          'Content-Type': 'application/json',
        }})
    // console.log('listproducts', response.data);
    return response.data;
  }

  getOrders = async () => {
    const response = await axios.get(`${this.baseURL}/listorders`)
    // console.log('listorders', response.data);
    return response.data;
  }

}

export default new API();

//https://tomoori.sa/dev/api/
