import { createSwitchNavigator, createBottomTabNavigator, createStackNavigator,} from "react-navigation";
import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image} from 'react-native';

import SplashScreen from "../routes/SplashScreen/SplashScreen";
import OnBoardingScreen from "../routes/OnBoardingScreen/OnBoardingScreen";
import Login from "../routes/Login/Login";
import RegistrationScreen from "../routes/Registration/RegistrationScreen";
import Home from "../routes/Home/Home";
import Orders from "../routes/Home/Orders";
import Search from "../routes/Home/Search";
import Profile from "../routes/Home/Profile";
import ProductDetails from "../routes/Home/ProductDetails";

import Notifications    from '../routes/Menu/Notifications';
import WhoUs            from '../routes/Menu/WhoUs';
import Explorer         from '../routes/Menu/Explorer';
import ContactUs        from '../routes/Menu/ContactUs';
import Branches         from '../routes/Menu/Branches';
import Setting          from '../routes/Menu/Setting';
import Help             from '../routes/Menu/Help';
import Privacy          from '../routes/Menu/Privacy';

import COLORS from '../global/colors/colors';
import TEXTS  from '../global/texts';
import IMAGES from '../global/images/images';

const TabNavigator = createBottomTabNavigator({
  	Orders: {
	    screen: Orders,
	    navigationOptions: () => ({
	    	tabBarIcon: ({ tintColor }) => {
	    	return (
	    		<Image source={IMAGES.TAB_ORDER} style={{width: 24, height: 24, tintColor: tintColor}}/>
	    		);
	    	}
	    })
	},
	Profile: {
	    screen: Profile,
	    navigationOptions: () => ({
	    	tabBarIcon: ({ tintColor }) => {
	    	return (
	    		<Image source={IMAGES.TAB_PROFILE} style={{width: 24, height: 24, tintColor: tintColor}}/>
	    		);
	    	}
	    })
	},
	Search: {
	    screen: Search,
	    navigationOptions: () => ({
	    	tabBarIcon: ({ tintColor }) => {
	    	return (
	    		<Image source={IMAGES.TAB_SEARCH} style={{width: 24, height: 24, tintColor: tintColor}}/>
	    		);
	    	}
	    })
	},
	Home: {
	    screen: Home,
	    navigationOptions: () => ({
	    	tabBarIcon: ({ tintColor }) => {
	    	return (
	    		<Image source={IMAGES.TAB_HOME} style={{width: 24, height: 24, tintColor: tintColor}}/>
	    		);
	    	}
	    })
	},  
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      showLabel: false,
      activeTintColor: COLORS.grapePurple,
      inactiveTintColor: COLORS.textDarkGrey,
    },
});

const switchNavigator = createSwitchNavigator({
    SplashScreen,
    OnBoardingScreen,
    Login,
    RegistrationScreen,
    Home,
    Orders,
    Search,
    Profile,
    TabNavigator,
    ProductDetails
});

export default switchNavigator;