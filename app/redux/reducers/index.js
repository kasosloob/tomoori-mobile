// import all reducers so that you can combine them in here.
import { combineReducers } from "redux";
import FriendListReducer from './friendListReducer';


const appReducer = combineReducers({
    FriendListReducer,
});

export default appReducer;
