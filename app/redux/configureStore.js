// @flow
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
// import middlewares here
import reducers from "./reducers";


const configureStore = (initialState = {}) => {
    return applyMiddleware(thunk)(createStore)(reducers, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
};

const store = configureStore();

export default store;
