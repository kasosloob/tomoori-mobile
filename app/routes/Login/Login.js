/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, TextInput, Image } from 'react-native'

import COLORS from '../../global/colors/colors'
import TEXTS from '../../global/texts';
import IMAGES from '../../global/images/images';
import API from '../../api/wooCommerce';

import Spinner from 'react-native-loading-spinner-overlay';


import { strings } from '../../I18n/I18n';
import I18n from 'react-native-i18n';
import en from '../../I18n/en';
import he from '../../I18n/he';


export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      usernameTxt: '',//'test8@gmail.com', 
      passwordTxt: '',//'qweqwe',
      spinner: false,
      language: true
    };
  }

  setLocale(x) {
    this.setState({ stateOfLocale: x });
    I18n.locale = x;
  }

  onLoginButtonClicked = async () => {
    // this.props.navigation.navigate("TabNavigator");
    const username = this.state.usernameTxt;
    const password = this.state.passwordTxt;

    this.setState({ spinner: !this.state.spinner });
    const response = await API.login(username, password);
    console.log('end login!!!!!!!!!!!!!!');
    this.setState({ spinner: !this.state.spinner });
    if (response.token != null) {
      this.props.navigation.navigate("TabNavigator");
    } else {
      setTimeout(() => {
        alert("Login Failed");
      }, 1000);
    }
  }

  onForgotPwdButtonClicked = () => {
    console.log('Forgot password button clicked');
  }

  onRegisterButtonClicked = () => {
    this.props.navigation.navigate("RegistrationScreen");
  }

  onSkipButtonClicked = () => {
    this.props.navigation.navigate("TabNavigator");
  }

  onFacebookButtonClicked = () => {
    console.log('Facebook button clicked');
  }

  onGooglePlusButtonClicked = () => {
    console.log('GooglePlus button clicked');
  }

  render() {
    const { language } = this.state
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
        <View style={styles.container}>

          <Spinner
            visible={this.state.spinner}
            textContent={'Log in...'}
            textStyle={{ color: '#FFF' }}
          />

          <TouchableOpacity style={{position:'absolute', right:2, top:17}}
            onPress={() => {
              language? this.setLocale('he'): this.setLocale('en');
              this.setState({language:!this.state.language})
            }}>
            <Image style={{ width: 30, height: 30 }} source={language?IMAGES.FLAG_EN:IMAGES.FLAG_AR} />
          </TouchableOpacity>

          <TouchableOpacity onPress={this.onSkipButtonClicked} style={{ height: 36, width: 50, marginTop: 20 }}>
            <Text style={{ fontSize: 20 }}>{strings('login.skip')}</Text>
          </TouchableOpacity>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ marginBottom: 15, fontWeight: "bold", fontSize: 18 }}>{strings('login.welcome')}</Text>
            <Text style={{ fontSize: 16, color: COLORS.purpleyGrey }}>{strings('login.please_sign_in')}</Text>
          </View>

          <View>
            <TextInput value={this.state.usernameTxt}
              placeholder={strings('login.email')}
              textColor={COLORS.purpleyGrey}
              keyboardType='email-address'
              autoCapitalize='none'
              textAlign={'right'}
              onChangeText={(usernameTxt) => this.setState({ usernameTxt })} />
            <View style={{ height: 1, backgroundColor: COLORS.grayBackgroundColor, marginBottom: 20, marginTop: 12 }}></View>
            <TextInput secureTextEntry={true}
              value={this.state.passwordTxt}
              placeholder={strings('login.password')}
              returnKeyType='done'
              textAlign={'right'}
              onChangeText={(passwordTxt) => this.setState({ passwordTxt })} />
            <View style={{ height: 1, backgroundColor: COLORS.grayBackgroundColor, marginTop: 12 }}></View>
          </View>

          <View>
            <TouchableOpacity onPress={this.onForgotPwdButtonClicked}>
              <View>
                <Text style={{ fontSize: 16, marginBottom: 20 }}>{strings('login.forgot_password')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onLoginButtonClicked}>
              <Text style={styles.buttonStyle}>{strings('login.login')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onRegisterButtonClicked}>
              <View style={{ flexDirection: 'row', marginTop: 20, alignSelf: "flex-end" }}>
                <Text style={{ fontWeight: "bold", marginRight: 10 }}>{strings('login.register_an_account')}</Text><Text style={{ fontSize: 16 }}>{strings('login.already_have_an_account')} </Text>
              </View>
            </TouchableOpacity>
          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 30,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
  },
  fbButtonView: {
    width: '100%',
    height: 50,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: "rgba(31, 87, 255, 0.15)",
    flexDirection: 'row',
    alignSelf: "flex-end",
    justifyContent: 'center',
    padding: 12,
  },
  gpButtonView: {
    width: '100%',
    height: 50,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: "rgba(253, 55, 59, 0.15)",
    flexDirection: 'row',
    alignSelf: "flex-end",
    justifyContent: 'center',
    padding: 12,
    marginVertical: 14,
  },
});