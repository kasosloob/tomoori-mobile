/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet, TouchableOpacity, Image } from "react-native";
import { connect } from "react-redux";
import Carousel, { Pagination } from "react-native-snap-carousel";

import COLORS from '../../global/colors/colors'
import TEXTS from '../../global/texts';
import IMAGES from '../../global/images/images';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight / 720;

const styles = StyleSheet.create({
  dotStyle: {
    width: 8,
    height: 8,
    borderRadius: 5,
    marginHorizontal: 1,
    backgroundColor: COLORS.grapePurple,
    marginTop: 20
  },
  inactiveDot: {
    width: 8,
    height: 8,
    borderRadius: 5,
    marginHorizontal: 1,
    backgroundColor: "white",
    marginTop: 20
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
  }
});

class OnBoardingScreen extends Component {

  componentWillMount(): void {
    const { width: viewportWidth, height: viewportHeight } = Dimensions.get("window");
    this.setState({ viewportWidth, viewportHeight, activeSlide: 0 })
  }

  _renderItem({ item, index }) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "flex-end", marginHorizontal: 30, marginTop: 80 }}>
        <Text style={{ fontWeight: "bold", fontSize: 20, color: COLORS.textDarkGrey, textAlign: "right" }}>{item.title}</Text>
        <Text style={{ fontSize: 14, color: COLORS.textGray, textAlign: "right", marginTop: 12 }}>{item.text}</Text>
        <Image source={item.image} style={{ width: (deviceHeight - 320) * 640 / 854, height: deviceHeight - 320, marginTop: 30, alignSelf: "center" }} />
      </View>
    );
  }

  SLIDERS = [
    {
      title: strings('onBoarding.title1'),
      image: IMAGES.ONBOARD1,
      text:  strings('onBoarding.text1')
    },
    {
      title: strings('onBoarding.title2'),
      image: IMAGES.ONBOARD2,
      text:  strings('onBoarding.text2')
    },
    {
      title: strings('onBoarding.title3'),
      image: IMAGES.ONBOARD3,
      text:  strings('onBoarding.text3')
    },
  ]

  Pagination = () => {
    const { activeSlide } = this.state;
    const { SLIDERS } = this;
    return (
      <Pagination
        dotsLength={SLIDERS.length}
        activeDotIndex={activeSlide}
        dotStyle={styles.dotStyle}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  };

  render() {
    const { viewportWidth, viewportHeight } = this.state;
    const carouselHeight = viewportHeight - 259;
    const { Pagination, SLIDERS } = this;
    const { activeSlide } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Carousel
          data={SLIDERS}
          renderItem={this._renderItem}
          sliderWidth={viewportWidth}
          itemWidth={viewportWidth}
          slideStyle={{ width: viewportWidth, height: carouselHeight }}
          onSnapToItem={(slideIndex) => this.setState({ activeSlide: slideIndex })}
        />
        {activeSlide === 2 &&
          <TouchableOpacity style={{ marginHorizontal: 40 }} onPress={() => this.props.navigation.navigate("Login")}>
            <Text style={styles.buttonStyle}>{strings('login.start_now')}</Text>
          </TouchableOpacity>
        }
        <Pagination style={{ paddingBottom: 100 }} />
      </View>
    );
  }
}

function mapStateToProps() {
  return {
  };
}


export default connect(mapStateToProps, null)(OnBoardingScreen);
