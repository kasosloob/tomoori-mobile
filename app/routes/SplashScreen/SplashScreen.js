/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import { View, Image } from "react-native";
import IMAGES from '../../global/images/images'


export default class SplashScreen extends Component {

  componentDidMount (): void {
    setTimeout(() => this.props.navigation.navigate("OnBoardingScreen"), 3000);
  }

  render() {
    return (
      <View style={{alignItems: "center", flex: 1, justifyContent: "center"}}>
        <Image source={IMAGES.LOGO} />
      </View>
    );
  }
}
