
'use strict';

import React, { Component } from "react";
import {
  View, Text, TouchableOpacity, StyleSheet, Dimensions, Image
} from 'react-native'
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720

export default class WhoUs extends Component {
    onMenuButtonClicked = () => {
      console.log('Menu button clicked');
      this.props.navigation.openDrawer();
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={styles.navbar}>
                <Text style={{fontWeight: "bold", fontSize: 18, color:'white'}}></Text>    
                <Text style={{fontWeight: "bold", fontSize: 18, color:'white'}}>{strings('menu.get_to_know_us')}</Text>            
                <TouchableOpacity onPress={this.onMenuButtonClicked}>
                  <Image source={IMAGES.IC_MENU} style={styles.menuImg}/>
                </TouchableOpacity> 
              </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 40,
    flexDirection: 'column',
    flex: 1
  },
  navbar: {
    height: 100,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: COLORS.whosusTopBackgroundColor,
    alignItems: 'center',
  },
  menuImg: {
    marginRight: 20,
    tintColor:'white'
  },
});