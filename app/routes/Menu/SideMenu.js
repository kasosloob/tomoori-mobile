
import React, { Component } from 'react';
import { Platform, StyleSheet, TouchableOpacity, Text, View, ImageBackground, Image, Button, TextInput, Dimensions, ScrollView, FlatList } from 'react-native';
import { createStackNavigator, createAppContainer, DrawerActions, NavigationActions } from "react-navigation";

import IMAGES from '../../global/images/images';
import COLORS from '../../global/colors/colors';
import PropTypes from 'prop-types';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight / 720;

class SideMenu extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data_list: [
        {
          title: strings('menu.home'), //home
          image: IMAGES.IC_HOME,
          content: 'Home'
        },
        {
          title: strings('menu.notification'), //notification
          image: IMAGES.IC_NOtIFY,
          content: 'Notifications'
        },
        {
          title: strings('menu.who_us'), //who us
          image: IMAGES.IC_GETKNOW,
          content: 'WhoUs'
        },
        {
          title: strings('menu.explorer'), //explorer
          image: IMAGES.IC_EXPLORER,
          content: 'Explorer'
        },
        {
          title: strings('menu.contact_us'), // contact us
          image: IMAGES.IC_CALL,
          content: 'ContactUs'
        },
        {
          title: strings('menu.branch'), //branch
          image: IMAGES.IC_BRANCH,
          content: 'Branches'
        },
        {
          title: strings('menu.settings'), //settings
          image: IMAGES.IC_SETTING,
          content: 'Setting'
        },
        {
          title: strings('menu.help'), //help
          image: IMAGES.IC_CONTACT,
          content: 'Help'
        },
        {
          title: strings('menu.terms_conditions'), //terms & conditions
          image: IMAGES.IC_SECURITY,
          content: 'Privacy'
        }

      ],
      username: strings('menu.username')
    }
  }

  onLogoutButtonClicked = () => {
    console.log('Logout button clicked');
    this.props.navigation.navigate("Login");
    // this.props.navigation.goBack();
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  }

  Doclick_viewDetail(index) {
    console.log('Doclick_viewDetail:', this.state.data_list[index].content);
    this.navigateToScreen(this.state.data_list[index].content);
  }

  renderItems(data_list, sectionIndex) {
    if (data_list.length != 0) {
      var i = -1;
      return data_list.map((data) => {
        i++;
        return (
          <Child_Product key={i} itemData={data} index={i} item_list={data_list} section_index={sectionIndex} click_viewDetail={this.Doclick_viewDetail} />
        )
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ alignItems: 'center', marginTop: 50 }}>
            <Image source={IMAGES.TEMP_AVATAR} style={{ width: 64, height: 64, borderRadius: 32 }} />
            <Text style={{ fontSize: res * 24, color: COLORS.textGray, textAlign: 'center' }}>{this.state.username}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontSize: res * 20, color: COLORS.textGray }}>{strings('menu.saudi')}</Text>
              <Image source={IMAGES.IC_PLACE} style={{ width: 16, height: 16 }} />
            </View>
          </View>

          <FlatList style={{ marginTop: 30 }}
            data={this.state.data_list}
            keyExtractor={(item, index) => index}
            extraData={this.state}
            renderItem={({ item, index }) =>
              <View key={index} style={{ width: 300, height: 40, alignItems: 'center', marginTop: 8 }}>
                <View style={{ marginRight: 20, width: 240, height: 35, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                  <TouchableOpacity onPress={this.navigateToScreen(this.state.data_list[index].content)}>
                    <Text style={{ fontSize: 20, fontWeight: '500', color: COLORS.textGray, width: 180, textAlign: 'right' }}>{item.title}</Text>
                  </TouchableOpacity>
                  <Image source={item.image} style={{ width: 24, height: 24 }} />
                </View>
              </View>
            }
            numColumns={1}
          />

          <TouchableOpacity onPress={this.onLogoutButtonClicked}>
            <View style={{ height: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
              <Text style={{ fontSize: 20, fontWeight: '500', color: COLORS.textGray, textAlign: 'center' }}>{strings('menu.join_us')}</Text>
              <Image source={IMAGES.IC_LOGOUT} style={{ width: 24, height: 24, marginLeft: 30 }} />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
});

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;
