
'use strict';

import React, { Component } from "react";
import {
  View, Text, TouchableOpacity, StyleSheet, Dimensions, Image, FlatList
} from 'react-native'
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720

export default class Explorer extends Component {
    constructor(props) {
      super(props);
      this.state = {
        flat_products : [
                  {
                      image : IMAGES.DSC,
                      name : strings('menu.explorer_title'),
                      is_new : 1,
                      date: "08 12, 2018",
                      is_like : 0
                  },
                  {
                      image : IMAGES.DSC_1079,
                      name : strings('menu.explorer_title'),
                      is_new : 0,
                      date: "08 12, 2018",
                      is_like : 0
                  },
                  {
                      image : IMAGES.DSC_1088,
                      name : strings('menu.explorer_title'),
                      is_new : 1,
                      date: "08 12, 2018",
                      is_like : 0
                  },
                  {
                      image : IMAGES.DSC_1092,
                      name : strings('menu.explorer_title'),
                      is_new : 0,
                      date: "08 12, 2018",
                      is_like : 0
                  },
                  {
                      image : IMAGES.DSC_1096,
                      name : strings('menu.explorer_title'),
                      is_new : 0,
                      date: "08 12, 2018",
                      is_like : 0
                  },
                  {
                      image : IMAGES.DSC_1102,
                      name : strings('menu.explorer_title'),
                      is_new : 0,
                      date: "08 12, 2018",
                      is_like : 0
                  }
              ]
        };
    }

    onMenuButtonClicked = () => {
      console.log('Menu button clicked');
      this.props.navigation.openDrawer();
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={styles.navbar}>
                <Text style={{fontWeight: "bold", fontSize: 18, color:'white'}}></Text>    
                <Text style={{fontWeight: "bold", fontSize: 18}}>{strings('menu.explorer')}</Text>            
                <TouchableOpacity onPress={this.onMenuButtonClicked}>
                  <Image source={IMAGES.IC_MENU} style={styles.menuImg}/>
                </TouchableOpacity> 
              </View>
              <FlatList
                    data= {this.state.flat_products}
                    keyExtractor={(item, index) => index.toString()}
                    extraData= {this.state}
                    renderItem={ ({item, index}) =>
                    <TouchableOpacity style={{width:deviceWidth/2.2, height:deviceHeight/3, borderRadius:5, marginLeft:(deviceWidth-deviceWidth/1.1)/3, marginTop:(deviceWidth-deviceWidth/1.1)/3}}>
                        <View style={{width:deviceWidth/2.2, height:deviceHeight/4.5, borderTopLeftRadius: 5, borderTopRightRadius: 5, backgroundColor:COLORS.grayBackgroundColor, alignItems:'center', justifyContent:'center'}}>
                            <Image source={item.image} style={{width: deviceWidth/2.2, height: deviceHeight/8, borderTopLeftRadius:5, borderTopRightRadius:5, alignSelf:'center'}} />
                        </View>
                        <View style={{justifyContent:'center', width:deviceWidth/2.2, height:deviceHeight/3-deviceHeight/4.5, backgroundColor:'#fff', borderBottomRightRadius:10, borderBottomLeftRadius:10}}>
                            <Text style={{fontSize:res*13, fontWeight:'400', textAlign:'right', marginTop:deviceHeight/50, marginRight:deviceHeight/50}}>{item.name}</Text>
                            <View style={{alignItems:'flex-end', justifyContent:'space-between', margin:deviceHeight/50}}>
                                <Text style={{fontWeight:'500', fontSize:res*13, color:COLORS.textGray}}>{item.date}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    }
                    numColumns={2}
                />          
            </View>
        );
    }
}

const styles = StyleSheet.create({
  navbar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  menuImg: {
    marginRight: 20,
  },
});