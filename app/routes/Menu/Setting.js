
'use strict';

import React, { Component } from "react";
import {
  View, Text, TouchableOpacity, StyleSheet, Dimensions, Image
} from 'react-native'
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720
import { strings } from '../../I18n/I18n';

export default class Setting extends Component {
    onMenuButtonClicked = () => {
      console.log('Menu button clicked');
      this.props.navigation.openDrawer();
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={styles.navbar}>
                <View style={styles.filterImg}>
                  <Image source={IMAGES.IC_BOOK} />
                </View>              
                <TouchableOpacity onPress={this.onMenuButtonClicked}>
                  <Image source={IMAGES.IC_MENU} style={styles.menuImg}/>
                </TouchableOpacity> 
              </View>
              <Text style={{fontWeight: "bold", fontSize: 18, marginBottom:50}}>{strings('menu.setting_screen')}</Text>            
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 40,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
  },
  navbar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  menuImg: {
    marginRight: 20,
  },
  filterImg: {
    flexDirection: 'row',
    marginLeft: 20,
  },
});