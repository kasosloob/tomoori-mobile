
'use strict';

import React, { Component } from "react";
import {
  View, Text, TouchableOpacity, StyleSheet, Dimensions, Image, ScrollView, TextInput
} from 'react-native'
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720
const tx_callus = strings('menu.callus');

export default class ContactUs extends Component {
    constructor(props) {
      super(props);
      this.state = {nameTxt: '', 
                    emailTxt: '', 
                    commentTxt: '',
                  };
    }

    onMenuButtonClicked = () => {
      console.log('Menu button clicked');
      this.props.navigation.openDrawer();
    }

    onSendMsgButtonClicked = () => {
      console.log('Send message button clicked');
    }

    render() {
        return (
          <ScrollView contentContainerStyle={{flexGrow: 1}} >
            <View style={{ flex: 1, alignItems: 'center'}}>
              <View style={styles.navbar}>
                <Text style={{fontWeight: "bold", fontSize: 18, color:'white'}}></Text>    
                <Text style={{fontWeight: "bold", fontSize: 18}}>{strings('menu.explorer')}</Text>            
                <TouchableOpacity onPress={this.onMenuButtonClicked}>
                  <Image source={IMAGES.IC_MENU} style={styles.menuImg}/>
                </TouchableOpacity> 
              </View>
              <Text style={{fontWeight:'500', fontSize:res*13, color:COLORS.textGray, marginHorizontal:20, textAlign:'right'}}>{tx_callus}</Text>
              <Text style={{fontWeight:'500', fontSize:res*15, color:COLORS.textGray, marginHorizontal:20, textAlign:'right', marginTop:24, alignSelf:'flex-end'}}>ابق علي تواصل</Text>

              <Text style={styles.label}>الاسم</Text>
              <TextInput style={styles.roundTxtFld}
                      value= {this.state.nameTxt}
                      placeholder=''
                      textColor={COLORS.purpleyGrey}
                      keyboardType='email-address' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(nameTxt) => this.setState({nameTxt})}/>
              <Text style={styles.label}>{strings('login.email')}</Text>
              <TextInput style={styles.roundTxtFld}
                      value= {this.state.emailTxt}
                      placeholder=''
                      textColor={COLORS.purpleyGrey}
                      keyboardType='email-address' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(emailTxt) => this.setState({emailTxt})}/>
              <Text style={styles.label}>{strings('menu.your_letter')}</Text>
              <TextInput style={styles.roundTxtView}
                      value= {this.state.commentTxt}
                      placeholder=''
                      textColor={COLORS.purpleyGrey}
                      keyboardType='email-address' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(commentTxt) => this.setState({commentTxt})}/>

              <TouchableOpacity onPress={ this.onSendMsgButtonClicked }>
                <Text style={styles.buttonStyle}>{strings('menu.send_your_message')}</Text>
              </TouchableOpacity>          
            </View>
          </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 40,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width:deviceWidth*0.9,
    height:52,
    marginTop:20
  },
  navbar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  menuImg: {
    marginRight: 20,
  },
  label: {
    fontWeight:'500', 
    fontSize:res*14, 
    color:COLORS.textGray, 
    marginHorizontal:20, 
    textAlign:'right', 
    marginTop:16, 
    alignSelf:'flex-end'
  },
  roundTxtFld: {
    borderWidth:1, 
    borderColor:COLORS.textGray, 
    borderRadius:6, 
    marginTop:12,
    width:deviceWidth*0.9,
    height:52,
  },
  roundTxtView: {
    borderWidth:1, 
    borderColor:COLORS.textGray, 
    borderRadius:6, 
    marginTop:12,
    width:deviceWidth*0.9,
    height:96,
  },
});