
'use strict';

import React, { Component } from "react";
import {
  View, Text, TouchableOpacity, StyleSheet, Dimensions, Image, FlatList
} from 'react-native'
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720

import { strings } from '../../I18n/I18n';

export default class Notifications extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data_list: [
          {
              title: '#123456  1',
              image: IMAGES.IC_Niti_RED,
              content: strings('menu.notify_content')
          },
          {
              title: '#123456  2',
              image: IMAGES.IC_Niti_RED,
              content: strings('menu.notify_content')
          },
          {
              title: '#123456  3',
              image: IMAGES.IC_Niti_RED,
              content: strings('menu.notify_content')
          },         
        ]
      }
    }

    onMenuButtonClicked = () => {
      console.log('Menu button clicked');
      this.props.navigation.openDrawer();
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={styles.navbar}>
                <TouchableOpacity style={{width:45, height:30, backgroundColor:COLORS.grapePurple, justifyContent: 'center', alignItems: 'center', borderRadius:5, marginLeft:deviceWidth*0.05}}>
                    <Text style={{color:'white', textAlign:'center'}}>3</Text>
                </TouchableOpacity>
                <Text style={{fontSize:18, fontWeight:'500', }}>{strings('menu.alerts')}</Text>          
                <TouchableOpacity onPress={this.onMenuButtonClicked}>
                  <Image source={IMAGES.IC_MENU} style={styles.menuImg}/>
                </TouchableOpacity>
              </View>
              <FlatList
                data= {this.state.data_list}
                keyExtractor={(item, index) => index}
                extraData= {this.state}
                renderItem={ ({item, index}) =>
                  <View key={index} style={{backgroundColor:COLORS.notiCellBackgroundColor,  width: deviceWidth -20, height: 96, alignItems: 'center', marginTop: 10, flexDirection: 'row'}}>              
                      <View style={{width: deviceWidth - 116, flexDirection:'column', marginVertical:8, paddingLeft:20}}>
                        <View style={{justifyContent: 'space-between', flexDirection:'row'}}>
                          <Text style={{fontSize:14, fontWeight:'500', color:COLORS.textGray, textAlign:'right'}}>p 12:55</Text>
                          <Text style={{fontSize:17, fontWeight:'500', textAlign:'right'}}>{item.title}</Text>
                        </View>
                        <Text style={{fontSize:14, fontWeight:'500', color:COLORS.textGray, textAlign:'right'}}>{item.content}</Text>
                      </View>
                      <View style={{width:96, height:96, justifyContent: 'center', alignItems: 'center'}}>
                        <Image source={item.image} style={{width:24, height:24}} />
                      </View>
                  </View>
                }
                numColumns={1}
              />           
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 40,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
  },
  navbar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  menuImg: {
    marginRight: 20,
  },
  filterImg: {
    flexDirection: 'row',
    marginLeft: 20,
  },
});