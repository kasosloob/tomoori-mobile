/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, TextInput, Image } from 'react-native';
import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import Spinner from 'react-native-loading-spinner-overlay';
import API from '../../api/wooCommerce';

import { strings } from '../../I18n/I18n';

export default class RegistrationScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
                  firstnameTxt: '', 
                  secondnameTxt: '', 
                  usernameTxt: '', 
                  phoneTxt: '',
                  genderTxt: '',
                  passwordTxt: '',
                  confirmpwdTxt: '',
                  spinner: false,
                };
  }

  onRegisterButtonClicked = async() => {
    const username = this.state.usernameTxt;
    const password = this.state.passwordTxt;

    this.setState({spinner: !this.state.spinner});
    const response = await API.registeruser(username, username, password);
    console.log('end register!!!!!!!!!!!!!!');
    this.setState({spinner: !this.state.spinner});
    if (response.response.code === 200) {
      this.props.navigation.navigate("TabNavigator");
    } else {
      setTimeout(() => {
        alert("Signup Failed");
      }, 1000);
    }
  }

  onLoginButtonClicked = () => {
    this.props.navigation.navigate("Login");
  }

  render() {
    return (
      <ScrollView  contentContainerStyle={{flexGrow: 1}}>
        <View style={styles.container}>
          <Spinner
            visible={this.state.spinner}
            textContent={'Sign up...'}
            textStyle={{color: '#FFF'}}
          />
          <View style={{justifyContent: 'center', alignItems: 'flex-end', textAlign:'right', marginTop:30}}>
            <Text style={{marginBottom: 15, fontWeight: "bold", fontSize: 18}}>{strings('login.register_account')}</Text>
          </View>

          <View>
            <View style={{flexDirection: 'row', alignItems:'space-between', width:'100%'}}>
              <View style={{marginRight:30}}>
                <TextInput value= {this.state.firstnameTxt}
                          placeholder={strings('login.email')}
                          textColor={COLORS.purpleyGrey}
                          keyboardType='default' 
                          autoCapitalize='none'
                          textAlign={'right'}
                          onChangeText={(firstnameTxt) => this.setState({firstnameTxt})}/>
                <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12, width:140}}></View>
              </View>
               <View>
                <TextInput value= {this.state.secondnameTxt}
                          placeholder={strings('login.forename')}
                          textColor={COLORS.purpleyGrey}
                          keyboardType='default' 
                          autoCapitalize='none'
                          textAlign={'right'}
                          onChangeText={(secondnameTxt) => this.setState({secondnameTxt})}/>
                <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12, width:140}}></View>
              </View>
            </View>

            <TextInput value= {this.state.usernameTxt}
                      placeholder={strings('login.email')}
                      textColor={COLORS.purpleyGrey}
                      keyboardType='email-address' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(usernameTxt) => this.setState({usernameTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12}}></View>

            <TextInput value= {this.state.phoneTxt}
                      placeholder={strings('login.phonenumber')}
                      textColor={COLORS.purpleyGrey}
                      keyboardType='phone-pad' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(phoneTxt) => this.setState({phoneTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12}}></View>

            <TextInput value= {this.state.genderTxt}
                      placeholder={strings('login.sex')}
                      textColor={COLORS.purpleyGrey}
                      keyboardType='default' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(genderTxt) => this.setState({genderTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12}}></View>

            <TextInput secureTextEntry={true} 
                      value= {this.state.passwordTxt} 
                      placeholder={strings('login.password')}
                      returnKeyType='done'
                      textAlign={'right'}
                      onChangeText={(passwordTxt) => this.setState({passwordTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12}}></View>

            <TextInput secureTextEntry={true} 
                      value= {this.state.confirmpwdTxt} 
                      placeholder={strings('login.confirm_pwd')}
                      returnKeyType='done'
                      textAlign={'right'}
                      onChangeText={(confirmpwdTxt) => this.setState({confirmpwdTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginTop:12}}></View>
          </View>
          
          <View>
            <TouchableOpacity onPress={ this.onRegisterButtonClicked }>
              <Text style={styles.buttonStyle}>{strings('login.registration')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={ this.onLoginButtonClicked }>
              <View style={{flexDirection: 'row', marginTop: 20, alignSelf:"flex-end", marginBottom:30}}>
                <Text style={{fontWeight: "bold", marginRight:10}}>{strings('login.log_in')}</Text><Text style={{fontSize: 16}}>{strings('login.already_have_an_account')}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 30,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
  }, 
});
