/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import {
  View, Text, TouchableOpacity, StyleSheet, Dimensions, Image
} from 'react-native'
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';
import COLORS from '../../global/colors/colors'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720

import API from '../../api/wooCommerce';
import Spinner from 'react-native-loading-spinner-overlay';
import { strings } from '../../I18n/I18n';

export default class Orders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            orders_failed:[],
            orders_complete:[],
            orders_shipping:[],
            orders_prepare:[],
            data_list : [
                {
                    date : "20/04/2019",
                    id : "19401",
                    cost : "188",
                    count : "3"
                },
                {
                    date : "09/03/2019",
                    id : "197781",
                    cost : "119",
                    count : "2"
                }
            ]
        };
    }

    componentWillMount (): void {
      this.getOrders();
    }

    getOrders = async() => {
      var response = await API.getOrders();
      response  = response.substr(0,response.length-1);
      response = JSON.parse(response);
      this.setState({spinner: !this.state.spinner});
      if (response !=null) {
        var array1 = [];
        var array2 = [];
        var array3 = [];
        var array4 = [];
        for(var i=0;i<response.length;i++) {
          var dict = {
              id: response[i].id,
              cost: response[i].total,
              currency: response[i].currency,
          };

          var date = new Date(response[i].date_created);
          let dateStr = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
          dict.date = dateStr;

          let status = response[i].status;
          if (status === 'failed') {
              array1.push(dict);
          } else if (status === 'complete') {
              array2.push(dict);
          } else if (status === 'processing') {
              array3.push(dict);
          } else if (status === 'cancelled') {
              array4.push(dict);
          } else {

          }          
        }
        this.setState({ orders_failed: array1,
                        orders_complete: array2,
                        orders_shipping: array3,
                        orders_prepare: array4,});

        console.log('===================');
        console.log(this.state.orders_failed);
      }
    }
    renderItems1(data_list) {
        if (data_list.length != 0) {
          var i = -1;
          return data_list.map((data)=>{
            i++;
            return (
              <Child1 key={i} itemData={data} index={i} item_list={data_list} />
            )
          })
        }
    }
    renderItems2(data_list) {
        if (data_list.length != 0) {
          var i = -1;
          return data_list.map((data)=>{
            i++;
            return (
              <Child2 key={i} itemData={data} index={i} item_list={data_list} />
            )
          })
        }
    }
    renderItems3(data_list) {
        if (data_list.length != 0) {
          var i = -1;
          return data_list.map((data)=>{
            i++;
            return (
              <Child3 key={i} itemData={data} index={i} item_list={data_list} />
            )
          })
        }
    }
    renderItems4(data_list) {
        if (data_list.length != 0) {
          var i = -1;
          return data_list.map((data)=>{
            i++;
            return (
              <Child4 key={i} itemData={data} index={i} item_list={data_list} />
            )
          })
        }
    }

    render() {
        return (
            <Container>
                <Tabs tabBarUnderlineStyle={{backgroundColor:COLORS.grapePurple, height:2}}>
                    <Tab heading={strings('home.canceled')} tabStyle={{backgroundColor:'white'}} activeTabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTextStyle={{color:COLORS.grapePurple}}>
                        {this.renderItems1(this.state.orders_failed)}
                    </Tab>
                    <Tab heading={strings('home.complete')} tabStyle={{backgroundColor:'white'}} activeTabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTextStyle={{color:COLORS.grapePurple}}>
                        {this.renderItems2(this.state.orders_complete)}
                    </Tab>
                    <Tab heading={strings('home.in_shipping')} tabStyle={{backgroundColor:'white'}} activeTabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTextStyle={{color:COLORS.grapePurple}}>
                        {this.renderItems3(this.state.orders_shipping)}
                    </Tab>
                    <Tab heading={strings('home.in_preparation')} tabStyle={{backgroundColor:'white'}} activeTabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTextStyle={{color:COLORS.grapePurple}}>
                        {this.renderItems4(this.state.orders_prepare)}
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}


class Child1 extends Component {
    constructor(props){
      super(props);
      this.state={
  
      }
    }

   
    render() {
      return (
        <View style={{margin:deviceWidth/200, backgroundColor:'white'}}>
            <View style={{width:deviceWidth*0.9, height:deviceHeight/5, marginLeft:deviceWidth*0.05}}>
                <Text style={{margin:deviceWidth*0.05}}>{this.props.itemData.date}</Text>
                <View style={{justifyContent:'space-between', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:deviceWidth/6, height:deviceHeight/30, backgroundColor:COLORS.tab1_buttonColor, borderRadius:5, marginLeft:deviceWidth*0.05}}>
                        <Text style={{color:'white', textAlign:'center'}}>{strings('home.canceled')}</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row', marginRight:deviceWidth*0.05}}>
                        <Text style={{marginRight:deviceWidth/20}}>#{this.props.itemData.id}</Text>
                        <Text style={{color:COLORS.textColor}}>{strings('home.the_demand')}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceWidth*0.05}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{marginRight:deviceWidth/20}}>{this.props.itemData.cost} {this.props.itemData.currency}</Text>
                        <Text style={{color:'#acacac'}}>{this.props.itemData.count} <Text style={{color:'#acacac'}}>{strings('home.elements')}</Text></Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={{color:COLORS.textColor}}>+{strings('home.the_details')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
              
            <View style={{width:deviceWidth*0.9, height:1, backgroundColor:COLORS.lineColor, marginLeft:deviceWidth*0.05}} />
          </View>
      )
    }
  }

  class Child2 extends Component {
    constructor(props){
      super(props);
      this.state={
  
      }
    }

   
    render() {
      return (
        <View style={{margin:deviceWidth/200, backgroundColor:'white'}}>
            <View style={{width:deviceWidth*0.9, height:deviceHeight/5, marginLeft:deviceWidth*0.05}}>
                <Text style={{margin:deviceWidth*0.05}}>{this.props.itemData.date}</Text>
                <View style={{justifyContent:'space-between', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:deviceWidth/6, height:deviceHeight/30, backgroundColor:COLORS.tab2_buttonColor, borderRadius:5, marginLeft:deviceWidth*0.05}}>
                        <Text style={{color:'white', textAlign:'center'}}>{strings('home.canceled')}</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row', marginRight:deviceWidth*0.05}}>
                        <Text style={{marginRight:deviceWidth/20}}>#{this.props.itemData.id}</Text>
                        <Text style={{color:COLORS.textColor}}>{strings('home.the_demand')}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceWidth*0.05}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{marginRight:deviceWidth/20}}>{this.props.itemData.cost} {this.props.itemData.currency}</Text>
                        <Text style={{color:'#acacac'}}>{this.props.itemData.count} <Text style={{color:'#acacac'}}>{strings('home.elements')}</Text></Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={{color:COLORS.textColor}}>+{strings('home.the_details')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
              
            <View style={{width:deviceWidth*0.9, height:1, backgroundColor:COLORS.lineColor, marginLeft:deviceWidth*0.05}} />
          </View>
      )
    }
  }

  class Child3 extends Component {
    constructor(props){
      super(props);
      this.state={
  
      }
    }

   
    render() {
      return (
        <View style={{margin:deviceWidth/200, backgroundColor:'white'}}>
            <View style={{width:deviceWidth*0.9, height:deviceHeight/5, marginLeft:deviceWidth*0.05}}>
                <Text style={{margin:deviceWidth*0.05}}>{this.props.itemData.date}</Text>
                <View style={{justifyContent:'space-between', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:deviceWidth/6, height:deviceHeight/30, backgroundColor:COLORS.tab3_buttonColor, borderRadius:5, marginLeft:deviceWidth*0.05}}>
                        <Text style={{color:'white', textAlign:'center'}}>{strings('home.canceled')}</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row', marginRight:deviceWidth*0.05}}>
                        <Text style={{marginRight:deviceWidth/20}}>#{this.props.itemData.id}</Text>
                        <Text style={{color:COLORS.textColor}}>{strings('home.the_demand')}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceWidth*0.05}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{marginRight:deviceWidth/20}}>{this.props.itemData.cost} {this.props.itemData.currency}</Text>
                        <Text style={{color:'#acacac'}}>{this.props.itemData.count} <Text style={{color:'#acacac'}}>{strings('home.elements')}</Text></Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={{color:COLORS.textColor}}>+{strings('home.the_details')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
              
            <View style={{width:deviceWidth*0.9, height:1, backgroundColor:COLORS.lineColor, marginLeft:deviceWidth*0.05}} />
          </View>
      )
    }
  }

  class Child4 extends Component {
    constructor(props){
      super(props);
      this.state={
  
      }
    }

   
    render() {
      return (
        <View style={{margin:deviceWidth/200, backgroundColor:'white'}}>
            <View style={{width:deviceWidth*0.9, height:deviceHeight/5, marginLeft:deviceWidth*0.05}}>
                <Text style={{margin:deviceWidth*0.05}}>{this.props.itemData.date}</Text>
                <View style={{justifyContent:'space-between', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:deviceWidth/6, height:deviceHeight/30, backgroundColor:COLORS.tab4_buttonColor, borderRadius:5, marginLeft:deviceWidth*0.05}}>
                        <Text style={{color:'white', textAlign:'center'}}>{strings('home.canceled')}</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row', marginRight:deviceWidth*0.05}}>
                        <Text style={{marginRight:deviceWidth/20}}>#{this.props.itemData.id}</Text>
                        <Text style={{color:COLORS.textColor}}>{strings('home.the_demand')}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceWidth*0.05}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{marginRight:deviceWidth/20}}>{this.props.itemData.cost} {this.props.itemData.currency}</Text>
                        <Text style={{color:'#acacac'}}>{this.props.itemData.count} <Text style={{color:'#acacac'}}>{strings('home.elements')}</Text></Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={{color:COLORS.textColor}}>+{strings('home.the_details')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
              
            <View style={{width:deviceWidth*0.9, height:1, backgroundColor:COLORS.lineColor, marginLeft:deviceWidth*0.05}} />
          </View>
      )
    }
  }
  