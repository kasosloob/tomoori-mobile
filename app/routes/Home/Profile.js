/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, TextInput, Image} from 'react-native'

import COLORS from '../../global/colors/colors'
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';

import { TextField } from "react-native-material-textfield";
import { strings } from '../../I18n/I18n';

export default class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {usernameTxt: '', 
                  passwordTxt: '',
                };
  }

  onLoginButtonClicked = () => {
    this.props.navigation.navigate("TabNavigator");
    // this.props.navigation.navigate("Orders");
  }

  onForgotPwdButtonClicked = () => {
    console.log('Forgot password button clicked');
  }

  onRegisterButtonClicked = () => {
    this.props.navigation.navigate("RegistrationScreen");
  }

  onFacebookButtonClicked = () => {
    console.log('Facebook button clicked');
  }

  onGooglePlusButtonClicked = () => {
    console.log('GooglePlus button clicked');
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}} >
        <View style={styles.container}>
          <View style={{height: 36}}>
            <Image source={IMAGES.IC_SETTING} style={{width:25 , height:25}}/>
          </View>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Image source={IMAGES.LOGO} style={{width:64 , height:64, borderRadius:32, borderColor:COLORS.grapePurple, borderWidth:1}}/>
            <Text style={{fontSize: 16, color: COLORS.purpleyGrey, marginTop:30}}>{strings('home.please_sign_in_continue')}</Text>
          </View>

          <View>
            <TextInput value= {this.state.usernameTxt}
                      placeholder={strings('login.email')}
                      textColor={COLORS.purpleyGrey}
                      keyboardType='email-address' 
                      autoCapitalize='none'
                      textAlign={'right'}
                      onChangeText={(usernameTxt) => this.setState({usernameTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginBottom:20, marginTop:12}}></View>
            <TextInput secureTextEntry={true} 
                      value= {this.state.passwordTxt} 
                      placeholder={strings('login.password')}
                      returnKeyType='done'
                      textAlign={'right'}
                      onChangeText={(passwordTxt) => this.setState({passwordTxt})}/>
            <View style={{height:1, backgroundColor:COLORS.grayBackgroundColor, marginTop:12}}></View>
          </View>
          
          <View>
            <TouchableOpacity onPress={ this.onForgotPwdButtonClicked }>
              <View>
                <Text style={{fontSize: 16, marginBottom:20}}>{strings('login.forgot_password')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={ this.onLoginButtonClicked }>
              <Text style={styles.buttonStyle}>{strings('login.login')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={ this.onRegisterButtonClicked }>
              <View style={{flexDirection: 'row', marginTop: 20, alignSelf:"flex-end"}}>
                <Text style={{fontWeight: "bold"}}>{strings('login.register_account')}</Text><Text style={{fontSize: 16}}>{strings('login.already_have_an_account')}</Text>
              </View>
            </TouchableOpacity>
          </View>          
        
          <View>
              <TouchableOpacity onPress={this.onFacebookButtonClicked}>
                <View style={styles.fbButtonView}>                
                  <Text style={{fontSize:16, fontWeight:'bold', color:'blue'}}>{strings('login.register_with_facebook')}</Text>
                  <Image source={IMAGES.IC_FACEBOOK} style={{width:25 , height:25 , marginLeft:20}}/>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.onGooglePlusButtonClicked}>
                <View style={styles.gpButtonView}>
                  <Text style={{fontSize:16, fontWeight:'bold', color:'red'}}>{strings('login.google_account')}</Text>
                  <Image source={IMAGES.IC_GOOGLE} style={{width:25 , height:25 , marginLeft:20}}/>                
                </View>
              </TouchableOpacity>
          </View>
        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 30,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
  }, 
  fbButtonView:{
    width:'100%',
    height:50, 
    borderRadius:10, 
    overflow: 'hidden', 
    backgroundColor: "rgba(31, 87, 255, 0.15)", 
    flexDirection: 'row', 
    alignSelf:"flex-end", 
    justifyContent: 'center',
    padding: 12,
  },
  gpButtonView:{
    width:'100%',
    height:50, 
    borderRadius:10, 
    overflow: 'hidden', 
    backgroundColor: "rgba(253, 55, 59, 0.15)", 
    flexDirection: 'row', 
    alignSelf:"flex-end", 
    justifyContent: 'center',
    padding: 12,
    marginVertical: 14,
  },
});