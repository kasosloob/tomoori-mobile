/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import {Platform, StyleSheet, TouchableOpacity, Text, View, FlatList, ImageBackground, Image, Button, TextInput, Dimensions, ScrollView } from 'react-native';

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';

import SideMenu         from '../Menu/SideMenu';
import API from '../../api/wooCommerce';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 40,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
  },
  navbar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  menuImg: {
    marginRight: 20,
  },
  filterImg: {
    flexDirection: 'row',
    marginLeft: 20,
  },
});

class ProductDetails extends Component {

  onMenuButtonClicked = () => {
    console.log('Menu button clicked');
    this.props.navigation.openDrawer();
  }

  constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            products:[],
    }
  }
    // componentWillMount (): void {
      // this.getProducts();
    // }

    render() {
        return (
            <View>
              <View style={styles.navbar}>
                <View style={styles.filterImg}>
                  <Image source={IMAGES.IC_BOOK} />
                  <Image source={IMAGES.IC_HEART} style={{marginLeft:24}}/>
                </View>              
                <TouchableOpacity onPress={this.onMenuButtonClicked}>
                  <Image source={IMAGES.IC_MENU} style={styles.IC_ALLOW_RIGHT}/>
                </TouchableOpacity> 
              </View>           
            </View>
        );
    }
}

export default ProductDetails;
