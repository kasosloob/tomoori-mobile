/**
 * @format
 * @flow
 */
'use strict';

import React, { Component } from "react";
import {Platform, StyleSheet, TouchableOpacity, Text, View, FlatList, ImageBackground, Image, Button, TextInput, Dimensions, ScrollView } from 'react-native';
import { createStackNavigator, createAppContainer, createDrawerNavigator, createSwitchNavigator, SafeAreaView } from "react-navigation";

import COLORS from '../../global/colors/colors';
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';

import Swiper from 'react-native-swiper';

import SideMenu         from '../Menu/SideMenu';
import Notifications    from '../Menu/Notifications';
import WhoUs            from '../Menu/WhoUs';
import Explorer         from '../Menu/Explorer';
import ContactUs        from '../Menu/ContactUs';
import Branches         from '../Menu/Branches';
import Setting          from '../Menu/Setting';
import Help             from '../Menu/Help';
import Privacy          from '../Menu/Privacy';

import API from '../../api/wooCommerce';
import Spinner from 'react-native-loading-spinner-overlay';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    marginHorizontal: 40,
    flexDirection: 'column',
    flex: 1
  },
  buttonStyle: {
    backgroundColor: COLORS.grapePurple,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
  },
  navbar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  menuImg: {
    marginRight: 20,
  },
  filterImg: {
    flexDirection: 'row',
    marginLeft: 20,
  },
});

class Home extends Component {

  onMenuButtonClicked = () => {
    console.log('Menu button clicked');
    this.props.navigation.openDrawer();
  }

  constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            products:[],
            ads_images : [
                IMAGES.ADS1, IMAGES.DSC, IMAGES.DSC_1079, IMAGES.DSC_1092
            ],
            list_products : [
                {
                    image : IMAGES.ajwah,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.khudry,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.majdoul,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.safawy,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.ajwah,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.khudry,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.majdoul,
                    name : strings('home.khadri_pass')
                },
                {
                    image : IMAGES.safawy,
                    name : strings('home.khadri_pass')
                }
            ],
            flat_products : [
                {
                    image : IMAGES.DSC,
                    name : strings('menu.explorer_title'),
                    is_new : 1,
                    cost: "180",
                    is_like : 0
                },
                {
                    image : IMAGES.DSC_1079,
                    name : strings('menu.explorer_title'),
                    is_new : 0,
                    cost: "180",
                    is_like : 0
                },
                {
                    image : IMAGES.DSC_1088,
                    name : strings('menu.explorer_title'),
                    is_new : 1,
                    cost: "180",
                    is_like : 0
                },
                {
                    image : IMAGES.DSC_1092,
                    name : strings('menu.explorer_title'),
                    is_new : 0,
                    cost: "180",
                    is_like : 0
                },
                {
                    image : IMAGES.DSC_1096,
                    name : strings('menu.explorer_title'),
                    is_new : 0,
                    cost: "180",
                    is_like : 0
                },
                {
                    image : IMAGES.DSC_1102,
                    name : strings('menu.explorer_title'),
                    is_new : 0,
                    cost: "180",
                    is_like : 0
                }
            ]
        };
    }

    componentWillMount (): void {
      this.getProducts();
    }

    getProducts = async() => {
      var response = await API.getProducts();
      response  = response.substr(0,response.length-1);
      response = JSON.parse(response);
      this.setState({spinner: !this.state.spinner});
      if (response !=null) {
        var array = [];
        for(var i=0;i<response.length;i++) {
            var dict = {
                id: response[i].id,
                name: response[i].name,
                price: response[i].price,
                is_new: 0
            };
            let images = response[i]["images"];
            if (images.length !==0) {
                let image = images[0]["src"];
                dict.image = {uri:image};
            } else {
                dict.image = null;
            }

          array.push(dict);
        }
        this.setState({ products: array });
      }
    }

    renderItems_rtl(data_list) {
        if (data_list.length != 0) {
          var i = -1;
          return data_list.map((data)=>{
            i++;
            return (
              <Child_Product_rtl key={i} itemData={data} index={i} item_list={data_list}/>
            )
          })
        }
    }

    scrollListToStart(contentWidth, contentHeight) {
        this.scrollView1.scrollTo({x: contentWidth});
    }

    render() {
        return (
            <View>
            <View style={styles.navbar}>
              <View style={styles.filterImg}>
                <Image source={IMAGES.IC_BOOK} />
                <Image source={IMAGES.IC_HEART} style={{marginLeft:24}}/>
              </View>              
              <TouchableOpacity onPress={this.onMenuButtonClicked}>
                <Image source={IMAGES.IC_MENU} style={styles.menuImg}/>
              </TouchableOpacity> 
            </View>
            <ScrollView
                style={{width:deviceWidth, height:deviceHeight-134}}
            >
                <Swiper dot={<View style={{borderWidth:1, 
                                           borderColor:COLORS.grapePurple, 
                                           borderRadius:4, 
                                           width:8, height:8, margin:2}} />}  
                        activeDotColor={COLORS.grapePurple} 
                        dotColor="gray"
                        containerStyle={{width:deviceWidth, height:deviceHeight/3, flexDirection:'row'}}
                        showsButtons={false} loop={false}
                        autoplay={false}
                        pagingEnabled={true}>
                    {
                        this.state.ads_images.map((item, index) =>
                            <View key={index} style={{width:deviceWidth, height:deviceHeight/4, alignSelf:'center'}}>
                                <View style={{flexDirection:'row', height:deviceHeight/4.5, alignItems:'center'}}>
                                    <Text style={{width:deviceWidth/1.9,  paddingRight:18, fontSize:res*25, color:COLORS.grapePurple, textAlign:'right'}}>{strings('home.luxury_dates_from_Madinah')}</Text>
                                    <View style={{width:deviceWidth-deviceWidth/1.9, height:deviceHeight/5}}>
                                        <Image source={item} style={{flex:1, width: undefined, height: undefined, resizeMode:'contain', marginLeft:-50}} />
                                    </View>
                                    
                                </View>
                                <Text style={{marginRight:deviceWidth/2+18, textAlign:'right', marginTop:-20}}>{strings('home.show_more')}</Text>
                            </View>
                        )
                    }
                </Swiper>

                
                <View>
                  <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <TouchableOpacity style={{marginLeft:deviceWidth/50}}>
                      <Text style={{fontSize:res*15, fontWeight:'500', marginTop:deviceHeight/50, color:'black'}}>{strings('home.show_all')}</Text>
                    </TouchableOpacity>
                    <Text style={{fontSize:res*15, fontWeight:'500', marginTop:deviceHeight/50, color:'black', marginRight:deviceWidth/50}}>{strings('home.Iamdates')}</Text>
                  </View>
                  <ScrollView
                    ref={ref => this.scrollView1 = ref}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    onContentSizeChange={this.scrollListToStart.bind(this)}
                    contentContainerStyle={{}}
                    style={{backgroundColor:'white'}}
                  >
                    <View style={{flexDirection:'row', backgroundColor:'white'}}>
                      {this.renderItems_rtl(this.state.list_products)}
                    </View>
                  </ScrollView>
                </View>


                <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:-50}}>
                    <TouchableOpacity style={{marginLeft:deviceWidth/50}}>
                        <Text style={{fontSize:res*15, fontWeight:'500', color:'black'}}>{strings('home.everyone_is_satisfied')}</Text>
                    </TouchableOpacity>
                    <Text style={{fontSize:res*15, fontWeight:'500',color:'black', marginRight:deviceWidth/50}}>{strings('home.new_store')}</Text>
                </View>

                <FlatList
                    data= {this.state.products}
                    keyExtractor={(item, index) => index.toString()}
                    extraData= {this.state}
                    renderItem={ ({item, index}) =>
                    <TouchableOpacity style={{width:deviceWidth/2.2, height:deviceHeight/3, borderRadius:5, marginLeft:(deviceWidth-deviceWidth/1.1)/3, marginTop:(deviceWidth-deviceWidth/1.1)/3}}>
                        <View style={{width:deviceWidth/2.2, height:deviceHeight/4.5, borderTopLeftRadius: 5, borderTopRightRadius: 5, backgroundColor:COLORS.grayBackgroundColor}}>
                            <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceWidth/20, marginBottom:-10}}>
                                <View style={{backgroundColor:'white', borderRadius:deviceWidth/30, width:deviceWidth/15, height:deviceWidth/15, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={IMAGES.heart} style={{width:deviceWidth/20, height:deviceWidth/20}} />
                                </View>
                                {item.is_new == 1 ? 
                                    <View style={{backgroundColor:COLORS.textBackgroundColor, width:deviceWidth/10, height:deviceHeight/35, borderRadius:5}}>
                                        <Text style={{textAlign:'center', color: 'white'}}>{strings('home.new')}</Text>
                                    </View>
                                :
                                    null
                                }
                                
                            </View>
                            <Image source={item.image} style={{width: deviceWidth/2.2, height: deviceHeight/8, borderTopLeftRadius:5, borderTopRightRadius:5}} />
                        </View>
                        <View style={{justifyContent:'center', width:deviceWidth/2.2, height:deviceHeight/3-deviceHeight/4.5, backgroundColor:'#fff', borderBottomRightRadius:10, borderBottomLeftRadius:10}}>
                            <Text style={{fontSize:res*13, fontWeight:'400', textAlign:'center', marginTop:deviceHeight/50}}>{item.name}</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceHeight/50}}>
                                <Image source={IMAGES.cart} style={{width:deviceWidth/20, height:deviceWidth/20}} />
                                <Text style={{fontWeight:'500', fontSize:res*18}}>$ {item.price}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    }
                    numColumns={2}
                />
                
            </ScrollView>
            </View>
        );
    }
}

class Child_Product_rtl extends Component {
    constructor(props){
      super(props);
      this.state={
  
      }
    }
  
    render() {
      return (
        <View style={{margin:deviceWidth/200, backgroundColor:'white'}}>
          <View style={{flexDirection:'row', alignItems:'center'}}>
            <View style={{width:deviceWidth/5.5, height:deviceHeight/4, }}>
              <TouchableOpacity style={{alignItems:'center'}}>
                <View style={{width:deviceWidth/5.5, height:deviceWidth/5.5, borderRadius:deviceWidth/15, backgroundColor:COLORS.grayBackgroundColor}}>
                  <Image source={this.props.itemData.image} style={{flex:1, width: undefined, height: undefined, resizeMode:'contain', borderRadius:5}} />
                </View>
              </TouchableOpacity>
              <Text style={{width:deviceWidth/5.5, fontSize:res*13, fontWeight:'400', color:'#545454', textAlign:'center'}}>{this.props.itemData.name}</Text>
            </View>
          </View>
        </View>
      )
    }
  }

const menuNavigator = createDrawerNavigator({
  Home: { screen: Home },
  Notifications: { screen: Notifications },
  WhoUs: { screen: WhoUs },
  Explorer: { screen: Explorer },
  ContactUs: { screen: ContactUs },
  Branches: { screen: Branches },
  Setting: { screen: Setting },
  Help: { screen: Help },
  Privacy: { screen: Privacy },
}, {
  contentComponent: SideMenu,
  drawerWidth: 320,
  drawerPosition: 'right',
});
export default menuNavigator; //createAppContainer(menuNavigator);
