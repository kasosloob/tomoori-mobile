
'use strict';

import React, { Component } from "react";
import {Platform, StyleSheet, TouchableOpacity, Text, View, FlatList, ImageBackground, Image, Button, TextInput, Dimensions, ScrollView } from 'react-native';
import { Container, Header, Content, Tab, Tabs, ScrollableTab } from 'native-base';

import { SearchBar } from 'react-native-elements';

import COLORS from '../../global/colors/colors'
import TEXTS  from '../../global/texts';
import IMAGES from '../../global/images/images';
import { strings } from '../../I18n/I18n';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const res = deviceHeight/720

import API from '../../api/wooCommerce';
import Spinner from 'react-native-loading-spinner-overlay';

export default class Search extends Component {
    constructor(props) {
    	super(props);
    	this.state = {
    		search: '',
    		spinner: false,
    		products:[],
	    	flat_products : [
	                {
	                    image : IMAGES.DSC,
	                    name : strings('menu.explorer_title'),
	                    is_new : 1,
	                    cost: "180",
	                    is_like : 0
	                },
	                {
	                    image : IMAGES.DSC_1079,
	                    name : strings('menu.explorer_title'),
	                    is_new : 0,
	                    cost: "180",
	                    is_like : 0
	                },
	                {
	                    image : IMAGES.DSC_1088,
	                    name : strings('menu.explorer_title'),
	                    is_new : 1,
	                    cost: "180",
	                    is_like : 0
	                },
	                {
	                    image : IMAGES.DSC_1092,
	                    name : strings('menu.explorer_title'),
	                    is_new : 0,
	                    cost: "180",
	                    is_like : 0
	                },
	                {
	                    image : IMAGES.DSC_1096,
	                    name : strings('menu.explorer_title'),
	                    is_new : 0,
	                    cost: "180",
	                    is_like : 0
	                },
	                {
	                    image : IMAGES.DSC_1102,
	                    name : strings('menu.explorer_title'),
	                    is_new : 0,
	                    cost: "180",
	                    is_like : 0
	                }
	            ]
        };
    }

    search = text => {
	    console.log(text);
	};

	clear = () => {
	    this.search.clear();
	};

	SearchFilterFunction(text) {
	    //TODO
	    this.setState({
	      search:text,
	    });
	}

	componentWillMount (): void {
      this.getProducts();
    }

    getProducts = async() => {
      var response = await API.getProducts();
      response  = response.substr(0,response.length-1);
      response = JSON.parse(response);
      // console.log('end getProducts!!!!!!!!!!!!!!', typeof response);
      this.setState({spinner: !this.state.spinner});
      if (response !=null) {
      	var array = [];
        for(var i=0;i<response.length;i++) {
            var dict = {
                id: response[i].id,
                name: response[i].name,
                price: response[i].price,
                is_new: 0
            };
            let images = response[i]["images"];
            if (images.length !==0) {
                let image = images[0]["src"];
                dict.image = {uri:image};
            } else {
                dict.image = null;
            }

        	array.push(dict);
        }
        this.setState({ products: array });

        console.log('===================');
        console.log(this.state.products);
      }
    }

    render() {
        return (
            <View>
            	 <View style={{justifyContent: 'center', flex: 1, backgroundColor:'white', 
            	 			marginBottom:50, marginTop: Platform.OS == 'ios'? 0 : 0}}>
            	 	<SearchBar
            	 	  style={{alignItems:'flex-end'}}
			          default
			          lightTheme={true}
			          searchIcon={{ size: 24 }}
			          onChangeText={text => this.SearchFilterFunction(text)}
			          onClear={text => this.SearchFilterFunction('')}
			          placeholder={strings('home.dates_of_medina')}
			          value={this.state.search}
			        />
            	 </View>
                <FlatList
                    style={{width:deviceWidth, height:deviceHeight-110}}
                    data= {this.state.products}
                    keyExtractor={(item, index) => index.toString()}
                    extraData= {this.state}
                    renderItem={ ({item, index}) =>
                    <TouchableOpacity style={{width:deviceWidth/2.2, height:deviceHeight/3, borderRadius:5, marginLeft:(deviceWidth-deviceWidth/1.1)/3, marginTop:(deviceWidth-deviceWidth/1.1)/3}}>
                        <View style={{width:deviceWidth/2.2, height:deviceHeight/4.5, borderTopLeftRadius: 5, borderTopRightRadius: 5, backgroundColor:COLORS.grayBackgroundColor}}>
                            <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceWidth/20, marginBottom:-10}}>
                                <View style={{backgroundColor:'white', borderRadius:deviceWidth/30, width:deviceWidth/15, height:deviceWidth/15, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={IMAGES.heart} style={{width:deviceWidth/20, height:deviceWidth/20}} />
                                </View>
                                {item.is_new == 1 ? 
                                    <View style={{backgroundColor:COLORS.textBackgroundColor, width:deviceWidth/10, height:deviceHeight/35, borderRadius:5}}>
                                        <Text style={{textAlign:'center', color: 'white'}}>{strings('home.new')}</Text>
                                    </View>
                                :
                                    null
                                }
                                
                            </View>
                            <Image source={item.image} style={{width: deviceWidth/2.2, height: deviceHeight/8, borderTopLeftRadius:5, borderTopRightRadius:5}} />
                        </View>
                        <View style={{justifyContent:'center', width:deviceWidth/2.2, height:deviceHeight/3-deviceHeight/4.5, backgroundColor:'#fff', borderBottomRightRadius:10, borderBottomLeftRadius:10}}>
                            <Text style={{fontSize:res*13, fontWeight:'400', textAlign:'center', marginTop:deviceHeight/50}}>{item.name}</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between', margin:deviceHeight/50}}>
                                <Image source={IMAGES.cart} style={{width:deviceWidth/20, height:deviceWidth/20}} />
                                <Text style={{fontWeight:'500', fontSize:res*18}}>$ {item.price}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    }
                    numColumns={2}
                />       
            </View>
        );
    }
}

  